# XeninUI.Frame <client>Client</client>
The `XeninUI.Frame` is the moma of basically all VGUI elements.

# Parent
Derives from [EditablePanel](https://wiki.facepunch.com/gmod/EditablePanel).

# Methods

## SetTitle
Sets the title of the frame.
```laux
XeninUI.Frame:SetTitle(title: string)
```
**Arguments**
1. New title of the frame.

---

## ShowCloseButton
Determines whether the Frame's close button is displayed.
```laux
XeninUI.Frame:ShowCloseButton(show: boolean)
```
**Arguments**
1. `false` hides the close button, this is `true` by default.

# Example
Creates an empty `XeninUI.Frame`.
```laux
local frame = vgui.Create("XeninUI.Frame")
frame:SetSize(960, 720)
frame:Center()
frame:MakePopup()
frame:SetTitle("Party")
```
Final Result  
![Output](https://i.imgur.com/VRm6SnL.png)