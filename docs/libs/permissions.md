# Permissions <shared>Shared</shared>
How works `XeninUI.Permissions` object.

# Methods

## canAccessFramework
```laux
XeninUI.Permissions:canAccessFramework(ply: Player)
```
**Arguments**
1. Player to check if he can access the framework.

## isAdmin
```laux
XeninUI.Permissions:isAdmin(ply: Player, level = 1)
```
**Arguments**
1. Player to check if he is admin.
2. **Optional** Level

## isSuperAdmin
```laux
XeninUI.Permissions:isSuperAdmin(ply: Player)
```
**Arguments**
1. Player to check if he is superadmin.

## isXeninDeveloper
```laux
XeninUI.Permissions:isXeninDeveloper(ply: Player)
```
**Arguments**
1. Player to check if he is a Xenin Developer.

# ConVars

## xenin_easy_permissions
```laux
-- Default
"xenin_easy_permissions": 1
```
If enabled, developpers can access the framework.